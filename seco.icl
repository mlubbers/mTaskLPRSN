module seco

import Data.Tuple
import iTasks

import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

motionSound :: SimpleSDSLens [(DateTime, (Bool, Bool))]
motionSound = sharedStore "motionSound" []

airTemp :: SimpleSDSLens [(DateTime, ((Int, Int), (Real, Real)))]
airTemp = sharedStore "motionSound" []

lmsdelaysds :: SimpleSDSLens Int
lmsdelaysds = sharedStore "lmsdelay" (1*60)

aqhtdelaysds :: SimpleSDSLens Int
aqhtdelaysds = sharedStore "aqhtdelay" (5*60)

lightReadingSds :: SimpleSDSLens Real
lightReadingSds = sharedStore "lightreading" 0.0

//main :: Task String
main = updateInformation [] {TCPSettings|host="192.168.1.162", port=8123, pingTimeout= ?None}
	>>? \spec->withDevice spec (\dev->liftmTask deviceTask dev >&> viewSharedInformation [])

deviceTask :: Main (MTask v (Bool)) |  mtask, dht, liftsds, LightSensor, AirQualitySensor v & fun () v
deviceTask
	=    DHT (DHT_SHT (i2c 0x45)) \dht->
	     PIR D4 \pir->
	     airqualitysensor (i2c 0x5B) \aqs->
//	     soundDetector D1 A0 \ss->
	     lightsensor (i2c 0x23) \ls->
	     liftsds \lmsdelay=lmsdelaysds
	  In liftsds \lightSds=lightReadingSds
	  In liftsds \aqhtdelay=aqhtdelaysds
	  In fun \wlight=(\ovalue->
			getSds lmsdelay
			>>~. \w->light` (BeforeSec w) ls
			>>*. [IfValue (\l->l !=. ovalue) (setSds lightSds)]
			>>=. bool o wlight
	) In fun \unoccupiedWatch=(\()->
		getSds lmsdelay
		>>~. \w->
		     light`  (BeforeSec w) ls
		.&&. interrupt high pir
//		.&&. soundPresence` (BeforeSec w) ss
		>>*. [IfValue (tupopen \(light, motion)->light >. lightLimit |. motion) rtrn]
	) In fun \aqht=(\()->
		getSds aqhtdelay
		>>~. \w->
		     co2` (BeforeSec w) aqs
		.&&. temperature` (BeforeSec w) dht
		.&&. humidity` (BeforeSec w) dht
	) In fun \occupiedWatch=(\()->
		rtrn true
	) In fun \watch=(\occupied->
		//You always go from occupied to non-occupied and the other way around
		If (occupied)
			(occupiedWatch   ())
			(unoccupiedWatch ())
		>>|. watch (not occupied)
	) In {main=watch true}
where
	lightLimit = lit 500.0

bool :: (MTask v Bool) -> MTask v Bool
bool a = a
